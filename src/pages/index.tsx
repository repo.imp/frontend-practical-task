import { Login } from "@/components/home";

export default function Home() {
  return (
    <main className="w-full flex">
      <Login />
      <div className="w-full flex flex-col items-center justify-center  bg-blue-500 ">
        <div className="flex flex-col">
          <h3 className="font-medium tracking-wide text-4xl">EXS</h3>
          <h3 className="capitalize font-medium tracking-wide">
            Looms Efficiency Software
          </h3>
        </div>
      </div>
    </main>
  );
}
