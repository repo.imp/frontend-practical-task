import Table from "@/components/dashboard/Table";
import { ProtectedLayout } from "@/layout";

const dashboard = () => {
  return (
    <ProtectedLayout>
      <section className="w-full p-6 min-h-screen">
        <Table />
      </section>
    </ProtectedLayout>
  );
};

export default dashboard;
