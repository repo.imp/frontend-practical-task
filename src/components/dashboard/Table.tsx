import { BASE_URL } from "@/config";
import { ICompany } from "@/types/company";
import { notify } from "@/utils";
import { Add, Delete, Edit, Search } from "@mui/icons-material";
import { Button, Checkbox, IconButton, TextField } from "@mui/material";
import { useEffect, useState } from "react";
import useFetch from "../hooks/useFetch";
import AddCompany from "./AddCompany";

const Table = () => {
  const [allCompany, setCompany] = useState<ICompany[]>([]);
  const [openDialog, setOpenDialog] = useState<boolean>(false);
  const [editData, setEditData] = useState<ICompany | undefined>();
  const [searchTitle, setSearchTitle] = useState("");
  const [reload, setReload] = useState(false);
  const { mutate } = useFetch();

  useEffect(() => {
    const timeout = setTimeout(() => {
      (async () => {
        try {
          let ACCESS_TOKEN = localStorage.getItem("ACCESS_TOKEN");
          const response = await fetch(
            BASE_URL +
              "/company" +
              (searchTitle ? `?searchTitle=${searchTitle}` : ""),
            {
              method: "GET",
              headers: {
                "Content-Type": "application/json",
                authorization: `Bearer ${ACCESS_TOKEN}`,
              },
            }
          );

          const result = await response.json();

          if (result?.success) setCompany(result?.data?.data);
        } catch (error) {}
      })();
    }, 500);

    return () => {
      clearTimeout(timeout);
    };
  }, [searchTitle, reload]);

  const handleDelete = async (id: string) => {
    try {
      const response = await mutate({
        path: "/company/" + id,
        method: "DELETE",
      });

      if (response?.status !== 200) throw new Error(response?.data?.message);

      notify.success(response?.data?.message);
      setReload((prev) => !prev);
    } catch (error) {
      notify.error(
        error instanceof Error ? error.message : "Something went wrong"
      );
    }
  };

  return (
    <>
      <AddCompany
        open={openDialog}
        onClose={() => {
          setOpenDialog(false);
          setEditData(undefined);
        }}
        data={editData}
      />
      <div className="relative overflow-x-auto flex flex-col ">
        <div className="w-full flex bg-white p-6 text-black items-center justify-between">
          <div className="flex items-center gap-4">
            <h3 className="font-bold tracking-wide text-4xl">Company</h3>
            <TextField
              InputProps={{ startAdornment: <Search /> }}
              placeholder="search here..."
              sx={{
                width: 500,
              }}
              onChange={(e) => setSearchTitle(e?.target?.value)}
              value={searchTitle}
            />
          </div>
          <Button
            startIcon={<Add />}
            className="!bg-theme !text-white !px-6 !py-3"
            onClick={() => {
              setEditData(undefined);
              setOpenDialog(true);
            }}
          >
            Add Company
          </Button>
        </div>
        <table className="w-full text-sm text-left rtl:text-right text-gray-500 ">
          <thead className="text-lg text-gray-700  p-4 bg-gray-100 ">
            <tr>
              <th scope="col" className="px-6 py-3">
                <Checkbox />
              </th>
              <th scope="col" className="px-6 py-3">
                Code
              </th>
              <th scope="col" className="px-6 py-3">
                Name
              </th>
              <th scope="col" className="px-6 py-3">
                Link
              </th>
              <th scope="col" className="px-6 py-3">
                Machine
              </th>
              <th scope="col" className="px-6 py-3">
                Frequency
              </th>
              <th scope="col" className="px-6 py-3">
                Contact Number
              </th>
              <th scope="col" className="px-6 py-3">
                Remark
              </th>
              <th scope="col" className="px-6 py-3">
                Action
              </th>
            </tr>
          </thead>
          <tbody>
            {allCompany?.map((item) => (
              <tr className="bg-white border-b " key={item?._id}>
                <th
                  scope="row"
                  className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap "
                >
                  <Checkbox />
                </th>
                <th
                  scope="row"
                  className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap "
                >
                  {item?.code}
                </th>
                <td className="px-6 py-4">{item?.name}</td>
                <td className="px-6 py-4">{item?.link}</td>
                <td className="px-6 py-4">{item?.machine}</td>
                <td className="px-6 py-4">{item?.frequency}</td>
                <td className="px-6 py-4">{item?.contactNumber}</td>
                <td className="px-6 py-4">{item?.remark}</td>
                <td className="px-6 py-4">
                  <div className="flex items-center gap-4">
                    <IconButton
                      className="!bg-theme !text-white "
                      onClick={() => {
                        setEditData(item);
                        setOpenDialog(true);
                        setReload((prev) => !prev);
                      }}
                    >
                      <Edit />
                    </IconButton>
                    <IconButton
                      className="!bg-red-500 !text-white "
                      onClick={() => {
                        handleDelete(item?._id);
                      }}
                    >
                      <Delete />
                    </IconButton>
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default Table;
