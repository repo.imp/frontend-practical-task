import { ICompany } from "@/types/company";
import { notify } from "@/utils";
import { Button, Dialog, MenuItem, Select, TextField } from "@mui/material";
import { useFormik } from "formik";
import * as Yup from "yup";
import useFetch from "../hooks/useFetch";

const formDetails = [
  {
    key: "1",
    name: "code",
    label: "Code",
    placeholder: "Enter Code",
    initialValue: "",
    validation: Yup.string().required("Code is required"),
    type: "text",
  },
  {
    key: "2",
    name: "name",
    label: "Name",
    placeholder: "Enter name",
    initialValue: "",
    validation: Yup.string().required("Name is required"),
    type: "text",
  },
  {
    key: "3",
    name: "machine",
    label: "Machine",
    placeholder: "Enter machine",
    initialValue: "",
    validation: Yup.string().required("Machine is required"),
    type: "number",
  },
  {
    key: "4",
    name: "frequency",
    label: "Frequency",
    placeholder: "Enter machine",
    initialValue: "",
    validation: Yup.string().required("Frequency is required"),
    options: [
      {
        key: "1",
        label: "Low",
        value: "LOW",
      },
      {
        key: "2",
        label: "Medium",
        value: "MEDIUM",
      },
      {
        key: "3",
        label: "High",
        value: "HIGH",
      },
    ],
    type: "select",
  },
  {
    key: "5",
    name: "contactNumber",
    label: "Contact Number",
    placeholder: "Enter contact number",
    initialValue: "",
    validation: Yup.string().required("Contact number is required"),
    type: "number",
  },
  {
    key: "6",
    name: "remark",
    label: "Remark",
    placeholder: "Remark number",
    initialValue: "",
    validation: Yup.string().required("Remark is required"),
    type: "text",
  },
  {
    key: "3",
    name: "link",
    label: "Link",
    placeholder: "Link",
    initialValue: "",
    validation: Yup.string().required("Link is required"),
    type: "text",
  },
];

const AddCompany = ({
  open,
  onClose,
  data,
}: {
  open: boolean;
  onClose: () => void;
  data?: ICompany;
}) => {
  const initialValues = formDetails.reduce((acc, { name, initialValue }) => {
    acc[name] = initialValue;
    return acc;
  }, {} as any);

  const validationSchema = formDetails.reduce((acc, { name, validation }) => {
    acc[name] = validation;
    return acc;
  }, {} as any);

  const { mutate } = useFetch();

  const formik = useFormik({
    initialValues: {
      ...initialValues,
      ...data,
    },
    validationSchema: Yup.object(validationSchema),
    onSubmit: async (values) => {
      try {
        const response = await mutate({
          method: data?._id ? "PATCH" : "POST",
          body: JSON.stringify(values),
          path: "/company" + (data?._id ? `/${data?._id}` : ""),
        });

        if (response?.status !== 200) throw new Error(response?.data?.message);

        notify.success(response?.data?.message);
        localStorage.setItem("ACCESS_TOKEN", response?.data?.token);
        onClose?.();
      } catch (error) {
        notify.error(
          error instanceof Error ? error.message : "Something went wrong"
        );
      }
    },
    enableReinitialize: true,
  });

  return (
    <Dialog open={open} onClose={onClose} maxWidth="lg" fullWidth>
      <div className="w-full grid grid-cols-2 gap-4 p-8">
        <h3 className="w-full text-4xl font-bold py-4 ">Add Company</h3>
        <h3 className="w-full text-4xl font-bold py-4 "></h3>
        {formDetails?.map((item) => (
          <div
            className=" cols-span-2 w-full flex flex-col gap-2"
            key={item?.key}
          >
            <h3 className=" text-lg font-bold tracking-wide">{item?.label}</h3>
            {item?.type === "select" ? (
              <Select
                name={item?.name}
                value={formik?.values[item?.name]}
                onChange={formik?.handleChange}
              >
                {item?.options?.map((inner) => (
                  <MenuItem key={item?.key} value={inner?.value}>
                    {inner?.label}
                  </MenuItem>
                ))}
              </Select>
            ) : (
              <TextField
                select={item?.type === "select"}
                fullWidth
                name={item?.name}
                value={formik?.values[item?.name]}
                onChange={formik?.handleChange}
                onBlur={formik?.handleBlur}
                error={Boolean(
                  formik.touched[item?.name] && formik?.errors[item?.name]
                )}
                helperText={
                  formik.touched[item?.name] &&
                  (formik?.errors[item?.name] as string)
                }
                size="medium"
                placeholder={item?.placeholder}
                type={item?.type}
              />
            )}
          </div>
        ))}
      </div>
      <div className="w-full flex items-center justify-center gap-4 p-4">
        <Button
          className="!bg-transparent !text-theme w-full !max-w-[150px] !border-2 !py-2 px-4"
          onClick={() => onClose?.()}
        >
          Cancel
        </Button>
        <Button
          className="!bg-theme !border !text-white w-full !max-w-[150px] !py-2 px-4"
          onClick={() => formik.handleSubmit()}
        >
          Save
        </Button>
      </div>
    </Dialog>
  );
};

export default AddCompany;
