import { Email, Key, Visibility, VisibilityOff } from "@mui/icons-material";
import { Button, IconButton, TextField } from "@mui/material";
import { useFormik } from "formik";
import { useState } from "react";

import { notify } from "@/utils";
import { useRouter } from "next/router";
import * as Yup from "yup";
import useFetch from "../hooks/useFetch";

const formDetails = [
  {
    key: "1",
    name: "email",
    icon: <Email />,
    label: "Email",
    placeholder: "Enter Email",
    initialValue: "",
    validation: Yup.string()
      .required("Email is required")
      .email("Email is invalid"),
    type: "email",
  },
  {
    key: "2",
    name: "password",
    icon: <Key />,
    label: "Password",
    placeholder: "Enter Password",
    initialValue: "",
    validation: Yup.string().required("Password is required"),
    type: "password",
  },
];

const Login = () => {
  const [isPassword, setIsPassword] = useState(true);

  const { push } = useRouter();

  const initialValues = formDetails.reduce((acc, { name, initialValue }) => {
    acc[name] = initialValue;
    return acc;
  }, {} as any);

  const validationSchema = formDetails.reduce((acc, { name, validation }) => {
    acc[name] = validation;
    return acc;
  }, {} as any);

  const { mutate } = useFetch();

  const formik = useFormik({
    initialValues,
    validationSchema: Yup.object(validationSchema),
    onSubmit: async (values) => {
      try {
        const response = await mutate({
          method: "POST",
          body: JSON.stringify(values),
          path: "/auth/login",
        });

        if (response?.status !== 200) throw new Error(response?.data?.message);

        notify.success(response?.data?.message);
        localStorage.setItem("ACCESS_TOKEN", response?.data?.token);
        push("/dashboard");
      } catch (error) {
        notify.error(
          error instanceof Error ? error.message : "Something went wrong"
        );
      }
    },
  });

  return (
    <div className="w-full flex flex-col items-center justify-center  min-h-screen bg-white">
      <div className=" flex flex-col w-full gap-4 max-w-[550px] ">
        <div className="flex flex-col gap-4 w-full">
          <h3 className=" tracking-wide text-5xl font-bold">Login</h3>
          <h3 className="font-medium tracking-wide w-full  text-lg">
            Welcome! Log in to your account
          </h3>
        </div>
        <div className="flex flex-col py-4 gap-4">
          {formDetails?.map((item) => (
            <div className="flex flex-col gap-2" key={item?.key}>
              <h3 className=" text-lg font-bold tracking-wide">
                {item?.label}
              </h3>
              <TextField
                InputProps={{
                  startAdornment: item?.icon,
                  endAdornment: item?.name === "password" && (
                    <IconButton onClick={() => setIsPassword((prev) => !prev)}>
                      {" "}
                      {isPassword ? <Visibility /> : <VisibilityOff />}{" "}
                    </IconButton>
                  ),
                }}
                name={item?.name}
                value={formik?.values[item?.name]}
                onChange={formik?.handleChange}
                onBlur={formik?.handleBlur}
                error={Boolean(
                  formik.touched[item?.name] && formik?.errors[item?.name]
                )}
                helperText={
                  formik.touched[item?.name] &&
                  (formik?.errors[item?.name] as string)
                }
                size="medium"
                placeholder={item?.placeholder}
                inputProps={{
                  className: "!pl-4",
                }}
                type={
                  !isPassword && item?.name === "password" ? "text" : item?.type
                }
              />
              {item?.name === "password" && (
                <small className="w-full text-lg text-right font-medium ">
                  Forgot password?{" "}
                </small>
              )}
            </div>
          ))}
        </div>

        <div className="w-full flex flex-col items-center gap-2 justify-center">
          <Button
            fullWidth
            className="!bg-blue-500 !text-white hover:!text-blue-500 hover:!bg-blue-50 !font-medium !py-3"
            onClick={() => formik.handleSubmit()}
          >
            Login
          </Button>

          <span className="flex items-center justify-center ">
            <h3 className="font-medium tracking-wide text-lg">
              Don't have an account?
            </h3>
            <h3 className="font-semibold tracking-wide text-blue-500 text-lg">
              Sign Up
            </h3>
          </span>
        </div>
      </div>
    </div>
  );
};

export default Login;
