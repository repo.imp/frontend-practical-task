import { BASE_URL } from "@/config";
import { useEffect, useState } from "react";

const useFetch = (path?: string) => {
  const [data, setData] = useState<any>(null);
  const [loading, setLoading] = useState(false);
  const [statusCode, setStatusCode] = useState<number | undefined>();

  const fetchData = async ({
    path,
    method,
    body,
  }: {
    path?: string;
    method: "GET" | "POST" | "PUT" | "DELETE" | "PATCH";
    body?: any;
  }) => {
    try {
      if (!path) return;
      setLoading(true);

      let ACCESS_TOKEN = localStorage.getItem("ACCESS_TOKEN");

      let API_URL = new URL(`${BASE_URL}${path}`);

      const headers: HeadersInit = {
        "Content-Type": "application/json",
      };
      ACCESS_TOKEN && (headers["authorization"] = `Bearer ${ACCESS_TOKEN}`);
      const options: RequestInit = {
        method: method,
        headers,
        body: body,
      };
      !body && delete options?.body;

      const response = await fetch(API_URL, options);
      setStatusCode(response?.status);

      let data = await response.json();

      if (data?.ACCESS_TOKEN) {
        localStorage?.setItem("ACCESS_TOKEN", data?.ACCESS_TOKEN);
      }
      setData(data);
      return { data, status: response?.status };
    } catch (error) {
      return { data: error, status: 400 };
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchData({ path: path, method: "GET" });
  }, [path, fetchData]);

  return {
    data,
    loading,
    mutate: fetchData,
    statusCode,
  };
};

export default useFetch;
