import { IUser } from "@/types/user";
import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  currentUser: {} as IUser,
  loading: true,
};

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    setCurrentUser: (state, action) => {
      state.currentUser = action.payload;
    },
    setLoadingState: (state, action) => {
      state.loading = action.payload;
    },
  },
});

export const { setCurrentUser, setLoadingState } = authSlice.actions;
export default authSlice.reducer;
