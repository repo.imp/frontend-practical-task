export interface ICompany {
  _id: string;
  code: string;
  name: string;
  link: string;
  machine: number;
  frequency: "LOW" | "MEDIUM" | "HIGH";
  contactNumber: string;
  remark: string;
  createdBy: ObjectId;
}
