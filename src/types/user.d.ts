export interface IUser {
  _id: string;
  displayName: string;
  email: string;
  password: string;
  token: string;
  verificationInfo: {
    code: string;
    expireAt: Date;
  };
}
