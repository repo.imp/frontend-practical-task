import useFetch from "@/components/hooks/useFetch";
import { setCurrentUser, setLoadingState } from "@/features/AuthSlice";
import { useStateDispatch, useStateSelector } from "@/redux/store";
import { notify } from "@/utils";
import { Dashboard } from "@mui/icons-material";
import Head from "next/head";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

type Props = {
  children: JSX.Element;
  title?: string;
};

const navBar = [
  {
    key: "1",
    label: "Dashboard",
    path: "/dashboard",
    icon: (
      <Dashboard className="text-3xl  text-theme group-hover:text-white transition-all duration-300 ease-in-out " />
    ),
  },
];

const ProtectLayout = ({ children, title = "Admin" }: Props) => {
  const [largeNav, setLargeNav] = useState(false);
  const router = useRouter();
  const { mutate } = useFetch();

  const dispatch = useStateDispatch();
  const user = useStateSelector((state) => state.auth);

  useEffect(() => {
    (async () => {
      try {
        const token = localStorage.getItem("ACCESS_TOKEN");
        if (!token) {
          router.push("/login");
        }

        const response = await mutate({
          path: "/auth/self",
          method: "GET",
        });

        if (response?.status !== 200) throw new Error(response?.data?.message);

        dispatch(setCurrentUser(response?.data?.data?.data));
        dispatch(setLoadingState(false));
      } catch (error) {
        notify.error(
          error instanceof Error ? error.message : "Something went wrong"
        );
      }
    })();
  }, []);

  return (
    <>
      <Head>
        <title>{title}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <>
        <main className={`min-h-screen  text-white relative ml-16`}>
          <header className={`h-16 bg-theme flex justify-end `}>
            <div className="flex h-16 items-center justify-between px-4">
              <div className="flex items-center gap-6">
                <button className="cursor-pointer text-lg btn-primary  font-medium tracking-wide ">
                  {user?.currentUser?.displayName}
                </button>
              </div>
            </div>
          </header>
          <div className=" bg-gray-300 w-full">
            {user?.loading ? <div>Loading</div> : children}
          </div>
          <div
            className={`min-h-screen bg-gray-800 transition-all ease-in-out duration-300 fixed left-0 top-0 ${
              largeNav ? "w-60" : " w-16"
            }  `}
            onMouseEnter={() => setLargeNav(true)}
            onMouseLeave={() => setLargeNav(false)}
          >
            <h3 className="font-semibold tracking-wide text-lg border-b text-center w-full p-4 text-theme">
              EXS
            </h3>
            <div className="flex flex-col gap-4 mt-4">
              {navBar?.map((item) => (
                <span
                  className="w-full flex items-center px-4 gap-4 py-2 bg-gray-800 group transition-all duration-300 ease-in-out hover:bg-theme/50 cursor-pointer icon-hover"
                  key={item?.key}
                  onClick={() => router.push(item?.path)}
                >
                  <span className="w-fit">{item?.icon}</span>

                  <h3
                    className={`font-medium tracking-wide text-theme text-left group-hover:text-white overflow-hidden transition-all duration-300 ease-in-out ${
                      largeNav ? "w-[10rem]" : "w-0"
                    } `}
                  >
                    {item?.label}
                  </h3>
                </span>
              ))}
            </div>
          </div>
        </main>
      </>
    </>
  );
};

export default ProtectLayout;
